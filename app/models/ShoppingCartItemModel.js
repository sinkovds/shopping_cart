/**
 * Created by dmitrysinkov on 19.02.15.
 */
//модель элемента корзины
var ShoppingCartItemModel = Backbone.Model.extend({

    //метод увеличения количества товара и перерсчет стоимости
    count_up: function(){
        this.set('count', this.get('count') + 1);
        this.set({
            total_cost : this.get('count') * this.get('product').get('price')
        });
    },
    //метод уменьшения количества товара и перерсчет стоимости
    count_down: function(){
        this.set('count', this.get('count') - 1);
        this.set({
            total_cost : this.get('count') * this.get('product').get('price')
        });
    }
});