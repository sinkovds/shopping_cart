/**
 * Created by dmitrysinkov on 19.02.15.
 */
// модель корзины товаров
var ShoppingCartModel = Backbone.Model.extend({

    initialize: function() {
        this.set({
            products: new ShoppingCartItemsCollection(),
            total_count: 0,
            total_price: 0
        });

        // в качестве интерфейса сохранения состояния выбраны куки
        // если они есть то восстанавливаем предыдущее сосостояние корзины
        var data_storage = $.cookie('shopping_cart');
        if(data_storage){
            var object = JSON.parse(data_storage);
            this.set({
                products: new ShoppingCartItemsCollection(),
                total_count: object.total_count,
                total_price: object.total_price
            });

            var obj = this;
            for(var product in object.products){
                obj.get('products').add(
                    {
                        id:object.products[product].id,
                        total_cost: object.products[product].total_cost,
                        count: object.products[product].count,
                        product: new ProductModel(object.products[product].product)
                    }
                )
            }
        }
    },
    //метод перерасчета состояния корзины
    recount: function () {
        var obj = this;
        var total_count = _.reduce(obj.get('products').models, function(count, item){
            return item.get('count') + count;
        }, 0);
        var total_price = _.reduce(obj.get('products').models, function(price, item){
            return item.get('total_cost') + price;
        }, 0);
        this.set({
            total_price: total_price,
            total_count: total_count
        });

        $.cookie('shopping_cart', JSON.stringify(this));
    },

    //метод доабвления товара в корзину
    add_product: function(product){
        var item = this.get('products').get(product.get('id'));

        if(item){
            item.count_up();
            item.set({
                product: product
            });
        }else{
            item = new ShoppingCartItemModel();
            item.set({
                id: product.get('id'),
                product: product,
                count : 1,
                total_cost: product.get('price')
            });
            this.get('products').add(item);
        }

        this.recount();
    },

    //метод удаления единыцы товара из корзины
    del_product: function(product){
        var item = this.get('products').get(product.get('id'));

        if(item){
            item.count_down();
            if(item.get('count') <= 0){
                this.get('products').remove(item);
            }
            this.recount();
        }
    },

    // метод удаления товара из корзины
    remove_product: function(product){
        var item = this.get('products').get(product.get('id'));
        if(item){
            this.get('products').remove(item);
            this.recount();
        }
    }
});