/**
 * Created by dmitrysinkov on 19.02.15.
 */
// представление отображения информации о наличии товара в корзине
var ShoppingCartInfoView = Backbone.View.extend({
    el: $('#shopping_cart_info'),
    render: function () {
        var template = _.template($("#shoppingcartTextTemplate").html())
        this.$el.html(template(this.model.toJSON()));
        return this;
    },

    // обработчик события на открытие окна списка товаров в корзине
    events: {
        "click .basket" : "open_shopping_cart"
    },
    open_shopping_cart: function(){
        $('#shopping_cart_container').show();
    }

});