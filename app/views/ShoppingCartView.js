/**
 * Created by dmitrysinkov on 19.02.15.
 */

// передставдение корзины
var ShoppingCartView = Backbone.View.extend({
    el: $('#shopping_cart_container'),
    tagName: "div",
    className: "product_item",
    template: $("#shoppingcartTemplate").html(),

    render: function () {
        var template = _.template(this.template);
        this.$el.html(template(this.model.toJSON()));
        var obj = this;
        _.each(this.model.get('products').models, function (item) {
            obj.renderItem(item);
        }, this);
        template = _.template($("#shoppingcartItogTemplate").html())
        this.$el.find('.basket_mini').append((template(this.model.toJSON())));
    },

    renderItem: function(item){
        var view = new ShoppingCartItemView({
            model: item
        });
        view.set_shopping_cart(this.model);
        this.$el.find('.basket_mini').append(view.render().el);
    },
    events: {
        "click .close" : "close_shopping_cart"
    },
    close_shopping_cart: function(){
        $('#shopping_cart_container').hide();
    }

});