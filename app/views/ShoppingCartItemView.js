/**
 * Created by dmitrysinkov on 19.02.15.
 */

// представдение элемента списка товаров в корзине

var ShoppingCartItemView = Backbone.View.extend({
    tagName: "li",
    className: "basket_mini_item",
    template: $("#shoppingcartItemTemplate").html(),

    render: function () {
        var template = _.template(this.template);
        this.$el.html(template(this.model.toJSON()));
        return this;
    },

    events: {
        "click .btn_plus" : "add_shopping_cart",
        "click .btn_minus" : "del_shopping_cart",
        "click .btn_del" : "remove_shopping_cart"
    },

    add_shopping_cart: function(){
        this.shopping_cart.add_product(this.model.get('product'));
    },

    del_shopping_cart: function(){
        this.shopping_cart.del_product(this.model.get('product'));
    },

    remove_shopping_cart: function(){
        this.shopping_cart.remove_product(this.model.get('product'));
    }

});