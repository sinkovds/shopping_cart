//представление списка продуктов
var ProductsView = Backbone.View.extend({
    el:$('#product_list'),

    //метод генерации предствления списка продуктов
    render: function () {
        var obj = this;
        _.each(this.products_collection.models, function (product) {
            obj.renderProduct(product);
        }, this);
        this.$el.append($('<li/>',{"class":"clear"}));
    },

    renderProduct: function (product) {
        var product_view = new ProductView({
            model: product
        });
        product_view.set_shopping_cart(this.shopping_cart);
        this.$el.append(product_view.render().el);
    }
});