/**
 * Created by dmitrysinkov on 19.02.15.
 */

// пердставление элемента списка товаров
var ProductView = Backbone.View.extend({
    tagName: "li",
    className: "product_item",
    template: $("#productTemplate").html(),


    //метод генерации представления элемента списка продуктов
    render: function () {
        var template = _.template(this.template);
        this.$el.html(template(this.model.toJSON()));
        return this;
    },

    //обработчик события на добавление товара в корзину
    events: {
        "click .product_plus" : "add_shopping_cart"
    },

    //метод добавления продукта в корзину
    add_shopping_cart: function(){
        this.shopping_cart.add_product(this.model);
    }
});