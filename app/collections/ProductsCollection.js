/**
 * Created by dmitrysinkov on 19.02.15.
 */

//коллекция товаров
var ProductsCollection = Backbone.Collection.extend({

    //задание урла откуда скачивать товарные позиции
    url: function() {
        return '/products.json';
    },
    model: ProductModel
});