/**
 * Created by dmitrysinkov on 18.02.15.
 */

// методы доавбления корзины и коллекции товаров в представдения

Backbone.View.prototype.set_shopping_cart = function(shopping_cart){
    this.shopping_cart = shopping_cart;
}

Backbone.View.prototype.set_products_collection = function(products_collection){
    this.products_collection = products_collection;
}

// создание объекта корзины
var shopping_cart = new ShoppingCartModel();

// создание коллекции товаров
var products_collection = new ProductsCollection();

//представдение корзины
var shopping_cart_view  = new ShoppingCartView({model:shopping_cart});

//представдение информации о корзине
var shopping_cart_infoview  = new ShoppingCartInfoView({model:shopping_cart});

$(function(){

    //создение представдения товаров и доавбление объекта корзины к представдению
    var products_view = new ProductsView();
    products_view.set_shopping_cart(shopping_cart);

    //после того как загрузится список товаров происходит отрисовка видов

    products_collection.fetch().done(function(){

        products_view.set_products_collection(products_collection);
        products_view.render();

        shopping_cart_view.render();

        //подпись на события изменения в корзине
        shopping_cart_view.listenTo(shopping_cart, "change", shopping_cart_view.render);


        shopping_cart_infoview.render();

        //подпись на события изменения в корзине
        shopping_cart_infoview.listenTo(shopping_cart, "change", shopping_cart_infoview.render);

    });
});